﻿using System;

namespace ndc
{
    public class NdcMessageHeader
    {
        public NdcMessageType type;
        public NdcMessageCommand command;
        public Int64 message_length;
        public string sender;
    }
}

