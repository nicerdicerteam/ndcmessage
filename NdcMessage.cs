﻿using System;
using System.Net.Sockets;
using System.Text;

namespace ndc
{
    public class NdcMessage
    {
        private NdcMessageHeader header;
        private string message;
        private bool empty;
        private byte[] data;
        private const int SENDER_LENGTH = 127;
        private int HEADER_SIZE;

        /* ------------------------------------------------------------------ */

        public NdcMessage ()
        {
            int size_type    = sizeof(NdcMessageType);
            int size_command = sizeof(NdcMessageCommand);
            int size_length  = sizeof(Int64);
            int size_sender  = SENDER_LENGTH + 1;
            HEADER_SIZE      = size_type + size_command + size_length + size_sender;

            this.empty          = true;
            this.header         = new NdcMessageHeader();
            this.header.type    = NdcMessageType.NDC_MESSAGE_TYPE_INVALID;
            this.header.command = NdcMessageCommand.NDC_MESSAGE_COMMAND_NONE;
            this.header.message_length = 0;
        }

        public NdcMessage (string message, string from) : this()
        {
            this.header.type   = NdcMessageType.NDC_MESSAGE_TYPE_CHAT;
            /* get actual length of the message in bytes */
            this.header.message_length = Encoding.UTF8.GetBytes(message).Length;
            this.header.sender = from;
            this.message       = message;
            this.empty         = false;
        }

        public NdcMessage (NdcMessageCommand command) : this()
        {
            this.header.type    = NdcMessageType.NDC_MESSAGE_TYPE_COMMAND;
            this.header.command = command;
            this.header.message_length = 0;
            /* leave sender and message text in case of a command message */
            this.empty          = false;
        }

        public bool send (Socket receiver)
        {
            if (this.is_empty())
                throw new Exception("Unable to send an empty message");

            Int64 data_size = this.serialize();
            return receiver.Send(this.data) == data_size;
        }

        public bool receive (Socket sender)
        {
            /* message must be empty! */
            if (!this.is_empty())
                throw new Exception("Message must be empty to receive");
            
            const int CHUNK_SIZE = 512;
            byte [] buffer       = new byte[CHUNK_SIZE];

            int total    = 0;
            int received = 0;

            /* receive the header of the message to determine message length */
            received = sender.Receive(buffer, HEADER_SIZE, SocketFlags.None);
            if (received != HEADER_SIZE)
                return false;

            /* determine message header */
            this.header.type    = (NdcMessageType)    BitConverter.ToInt32(buffer, 0);
            this.header.command = (NdcMessageCommand) BitConverter.ToInt32(buffer, 4);
            this.header.message_length = BitConverter.ToInt64(buffer, 8);
            this.data           = new byte[this.header.message_length];
            /*
             * Determine lenght of sender string (maximum is SENDER_LENGTH)
             */
            int sender_length = 0;
            for (int i = 0; i < SENDER_LENGTH; i++)
            {
                if (buffer [16 + i] == '\0')
                    break;
                else
                    sender_length++;
            }
            this.header.sender = Encoding.UTF8.GetString(buffer, 16, sender_length);

            /* receive the message body */
            while (total < this.header.message_length)
            {
                int message_length = (int)this.header.message_length;
                int receive_length = message_length > CHUNK_SIZE ? CHUNK_SIZE : message_length;
                /* clear buffer before each receive() call */
                Array.Clear(buffer, 0, buffer.Length);
                received = sender.Receive(buffer, receive_length, SocketFlags.None);
                /* break if no data was received */
                if (received == 0) break;
                /* copy received data to internal message buffer */
                Buffer.BlockCopy(buffer, 0, this.data, total, received);
                total += received;
            }

            this.message = Encoding.UTF8.GetString(this.data);

            return true;
        }

        public bool is_empty ()
        {
            return this.empty;
        }

        public bool is_command ()
        {
            return this.header.type == NdcMessageType.NDC_MESSAGE_TYPE_COMMAND;
        }

        public NdcMessageType get_type ()
        {
            return this.header.type;
        }

        public NdcMessageCommand get_command ()
        {
            return this.header.command;
        }

        public Int64 get_length ()
        {
            return this.header.message_length;
        }

        public string get_sender ()
        {
            return this.header.sender;
        }

        public byte [] get_raw ()
        {
            this.serialize();
            return data;
        }

        public string get_message ()
        {
            return message;
        }


        /* ------------------------------------------------------------------ */

        private int append_to_array(Array src, Array dst, int offset, int count)
        {
            Buffer.BlockCopy(src, 0, dst, offset, count);
            return offset + count;
        }

        private Int64 serialize()
        {
            int size_type    = sizeof(NdcMessageType);
            int size_command = sizeof(NdcMessageCommand);
            int size_length  = sizeof(Int64);
            int size_sender  = SENDER_LENGTH + 1;
            int size_header  = size_type + size_command + size_length + size_sender;

            Int64 data_size = HEADER_SIZE + this.header.message_length;
            this.data       = new byte[data_size];
            int offset      = 0;
            
            offset = this.append_to_array(
                BitConverter.GetBytes((int) this.header.type), this.data,
                offset, size_type
            );
            offset = this.append_to_array(
                BitConverter.GetBytes((int) this.header.command), this.data,
                offset, size_command
            );
            offset = this.append_to_array(
                BitConverter.GetBytes(this.header.message_length), this.data,
                offset, size_length
            );

            if (this.header.type == NdcMessageType.NDC_MESSAGE_TYPE_CHAT)
            {
                this.append_to_array(
                    Encoding.UTF8.GetBytes(this.header.sender), this.data,
                    offset, this.header.sender.Length
                );
                this.append_to_array(
                    Encoding.UTF8.GetBytes(this.message), this.data,
                    HEADER_SIZE, (int) this.header.message_length
                );
            }

            return data_size;
        }
    }
}
