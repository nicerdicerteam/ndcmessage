﻿using System;
//using System.IO;
using System.Net.Sockets;

namespace ndc
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Console.WriteLine("## NdcMessage -- Test ##");

            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, 0);
            server.Connect("localhost", 7000);

            NdcMessage mr = new NdcMessage();
            if (mr.receive (server))
                Console.WriteLine(mr.get_sender() + "> " + mr.get_message());

            NdcMessage ms = new NdcMessage("Test 123, special chars: äöüß€", "test-user");
            //NdcMessage ms = new NdcMessage(NdcMessageCommand.NDC_MESSAGE_COMMAND_DISCONNECT);

            //File.WriteAllBytes("message.bytes", ms.get_raw());

            Console.WriteLine(ms.send(server));
        }
    }
}
