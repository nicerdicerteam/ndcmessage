﻿using System;

namespace ndc
{
    public enum NdcMessageType
    {
        NDC_MESSAGE_TYPE_INVALID,
        NDC_MESSAGE_TYPE_COMMAND,
        NDC_MESSAGE_TYPE_CHAT
    }
}

