﻿using System;

namespace ndc
{
    public enum NdcMessageCommand
    {
        NDC_MESSAGE_COMMAND_NONE,
        NDC_MESSAGE_COMMAND_DISCONNECT,
        NDC_MESSAGE_COMMAND_SHUTDOWN,
        NDC_MESSAGE_COMMAND_KILL_CLIENT,
        NDC_MESSAGE_COMMAND_CLIENT_LIST
    }
}

